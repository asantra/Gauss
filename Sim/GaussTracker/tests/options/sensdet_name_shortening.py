#!env python
from Gaudi.Configuration import *
from Configurables import GiGaSensDetTracker
from GaudiPython.Bindings import AppMgr
import cppyy
sensdet = GiGaSensDetTracker( 'ToolSvc.Hello', OutputLevel = -10)
bye = sensdet.addTool(GiGaSensDetTracker, 'Bye')
bye.OutputLevel = -10

gaudi = AppMgr()
blub = gaudi.toolSvc().create('GiGaSensDetTracker', 'Hello')
blub = gaudi.toolSvc().create('GiGaSensDetTracker', 'Hello.Bye')
gaudi.initialize()
