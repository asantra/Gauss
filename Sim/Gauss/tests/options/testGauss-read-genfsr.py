from Gauss.Configuration import *
from Configurables import GaudiSequencer

seqGenFSR = GaudiSequencer("GenFSRSeq")
seqGenFSR.Members += ["GenFSRRead", "GenFSRLog"]

ApplicationMgr().TopAlg += [seqGenFSR]

from Configurables import LHCbApp
LHCbApp().EvtMax = -1
LHCbApp().Simulation = True

#--Set database tags
LHCbApp().DDDBtag   = "dddb-20170721-2"
LHCbApp().CondDBtag = "sim-20160321-2-vc-md100"

EventSelector().Input += ["DATAFILE='PFN:genFSR_2012_Gauss_created.xgen' TYP='POOL_ROOTTREE' OPT='READ'"]

