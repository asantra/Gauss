from Configurables import Gauss, GaussRedecayMergeAndClean
Gauss().SplitSim = True
GaussRedecayMergeAndClean().OutputLevel = 2

from Gauss.Configuration import GenInit, LHCbApp
#--Number of events
nEvts = 5
LHCbApp().EvtMax = nEvts

GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 1
GaussGen.RunNumber = 1082
