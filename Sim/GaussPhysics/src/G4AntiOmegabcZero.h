#ifndef G4AntiOmegabcZero_h
#define G4AntiOmegabcZero_h 1

#include "Geant4/globals.hh"
#include "Geant4/G4ios.hh"
#include "Geant4/G4ParticleDefinition.hh"

// ######################################################################
// ###                         AntiOmegabcZero                        ###
// ######################################################################

class G4AntiOmegabcZero : public G4ParticleDefinition
{
 private:
  static G4AntiOmegabcZero * theInstance ;
  G4AntiOmegabcZero( ) { }
  ~G4AntiOmegabcZero( ) { }


 public:
  static G4AntiOmegabcZero * Definition() ;
  static G4AntiOmegabcZero * AntiOmegabcZeroDefinition() ;
  static G4AntiOmegabcZero * AntiOmegabcZero() ;
};


#endif
