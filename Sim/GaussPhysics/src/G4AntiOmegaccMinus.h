#ifndef G4AntiOmegaccMinus_h
#define G4AntiOmegaccMinus_h 1

#include "Geant4/globals.hh"
#include "Geant4/G4ios.hh"
#include "Geant4/G4ParticleDefinition.hh"

// ######################################################################
// ###                         AntiOmegaccMinus                        ###
// ######################################################################

class G4AntiOmegaccMinus : public G4ParticleDefinition
{
 private:
  static G4AntiOmegaccMinus * theInstance ;
  G4AntiOmegaccMinus( ) { }
  ~G4AntiOmegaccMinus( ) { }


 public:
  static G4AntiOmegaccMinus * Definition() ;
  static G4AntiOmegaccMinus * AntiOmegaccMinusDefinition() ;
  static G4AntiOmegaccMinus * AntiOmegaccMinus() ;
};


#endif
