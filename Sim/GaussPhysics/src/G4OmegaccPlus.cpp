// $Id: G4OmegaccPlus.cpp,v 1.1 2018-09-27 22:47:24 Jingyi Xu Exp $

#include "G4OmegaccPlus.h"
#include "Geant4/G4ParticleTable.hh"

// ######################################################################
// ###                      OmegaccPlus                        ###
// ######################################################################

G4OmegaccPlus * G4OmegaccPlus::theInstance = 0 ;

G4OmegaccPlus * G4OmegaccPlus::Definition()
{
  if (theInstance !=0) return theInstance;
  const G4String name = "omega_cc+";
  // search in particle table
  G4ParticleTable* pTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* anInstance = pTable->FindParticle(name);
  if (anInstance ==0)
  {
  // create particle
  //
  //    Arguments for constructor are as follows
  //               name             mass          width         charge
  //             2*spin           parity  C-conjugation
  //          2*Isospin       2*Isospin3       G-parity
  //               type    lepton number  baryon number   PDG encoding
  //             stable         lifetime    decay table
  //             shortlived      subType    anti_encoding
    anInstance = 
      new G4ParticleDefinition( name ,      3.738*CLHEP::GeV,  5.e-10*CLHEP::MeV ,  +1.*CLHEP::eplus ,
                                1,          +1,           0,
                                0,           0,           0,
                                "baryon",   0,            1,            4432,
                                false,      0.160e-3*CLHEP::ns,  NULL,
                                false,      "omega_cc" );
  }
  theInstance = reinterpret_cast<G4OmegaccPlus*>(anInstance);
  return theInstance;
}

G4OmegaccPlus * G4OmegaccPlus::OmegaccPlusDefinition() {
  return Definition( ) ;
}

G4OmegaccPlus * G4OmegaccPlus::OmegaccPlus() {
  return Definition( ) ;
}
