// Include files 



#include "GaussCherenkov/CkvSensDet.h"
#include "GaussCherenkov/CkvStdSensDet.h"
#include "GaussCherenkov/CkvGrandSensDet.h"


#include "GaussCherenkov/GetMCCkvHitsAlg.h"
#include "GaussCherenkov/GetMCCkvOpticalPhotonsAlg.h"
#include "GaussCherenkov/GetMCCkvSegmentsAlg.h"
#include "GaussCherenkov/GetMCCkvTracksAlg.h"


// Declaration of the Tool Factory

DECLARE_COMPONENT( CkvSensDet )
DECLARE_COMPONENT( CkvStdSensDet )
DECLARE_COMPONENT( CkvGrandSensDet )

DECLARE_COMPONENT( GetMCCkvHitsAlg )
DECLARE_COMPONENT( GetMCCkvOpticalPhotonsAlg )
DECLARE_COMPONENT( GetMCCkvSegmentsAlg )
DECLARE_COMPONENT( GetMCCkvTracksAlg )




