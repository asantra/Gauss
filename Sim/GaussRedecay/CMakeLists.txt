################################################################################
# Package: GaussRedecay
################################################################################
gaudi_subdir(GaussRedecay v1r0)

gaudi_depends_on_subdirs(Gen/Generators
                         Event/MCEvent
                         Event/GenEvent
                         Event/PhysEvent
                         Sim/GiGa)

find_package(Boost)
find_package(CLHEP)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS} ${Geant4_INCLUDE_DIRS})

gaudi_add_module(GaussRedecay
                 src/*.cpp
                 LINK_LIBRARIES GenEvent MCEvent PhysEvent GiGaLib)

gaudi_install_headers(GaussRedecay)
