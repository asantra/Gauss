#ifndef      GIGACNV_GIGAIsotopeCnv_H
#define      GIGACNV_GIGAIsotopeCnv_H  1 

/// from STL 
#include <set>
/// base class from GiGaCnv
#include "GiGaCnv/GiGaCnvBase.h"
#include "GiGaCnv/GiGaLeaf.h"
///
class Material;
class Mixture;
class Element;
class Isotope;

/** @class GiGaIsotopeCnv GiGaIsotopeCnv.h GiGa/GiGaIsotopeCnv.h

    Converter of Isotope class to Geant4

    @author  Vanya Belyaev
*/

class GiGaIsotopeCnv: public GiGaCnvBase
{
 public: 
  /// Standard Constructor
  GiGaIsotopeCnv( ISvcLocator* );
  /// Standard (virtual) destructor
  virtual ~GiGaIsotopeCnv();
  ///
 public:
  /// Create representation
  StatusCode createRep(DataObject*     Object  ,
                       IOpaqueAddress*& Address ) override;
  /// Update representation
  StatusCode updateRep(IOpaqueAddress*  Address, DataObject*   Object) override;
  /// Class ID for created object == class ID for this specific converter
  static const CLID&          classID();
  /// storage Type
  static unsigned char storageType() ;
  ///

private:

  GiGaLeaf m_leaf;

};

// ============================================================================
// End
// ============================================================================
#endif   //     __GIGA_GEOMCONVERSION_GIGAIsotopeCnv_H__
// ============================================================================



