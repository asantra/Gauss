################################################################################
# Package: GaussCalo
################################################################################
gaudi_subdir(GaussCalo v10r1)

gaudi_depends_on_subdirs(Det/CaloDet
                         Det/CaloDetXmlCnv
                         Event/MCEvent
                         Kernel/LHCbKernel
                         Sim/GaussTools)

find_package(AIDA)

find_package(Boost)
find_package(CLHEP)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS} ${Geant4_INCLUDE_DIRS})

gaudi_add_module(GaussCalo
                 src/*.cpp
                 INCLUDE_DIRS AIDA
                 LINK_LIBRARIES CaloDetLib MCEvent LHCbKernel GaussToolsLib)

gaudi_env(SET GAUSSCALOOPTS \${GAUSSCALOROOT}/options)
