// $Id: ConversionFilter.h,v 1.3 2008-05-07 09:54:20 gcorti Exp $
#ifndef CONVERSIONFILTER_H 
#define CONVERSIONFILTER_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class ConversionFilter ConversionFilter.h
 *  Filter on gamma conversions. Used after the simulation stage of Gauss to only write events that have a gamma conversion that fulfills certain criteria.
 * 
 *  Parameters:
 *  - Mother: Mother up the decay tree the gamma conversion has to originate from. Use DecayDescriptor symbols, like B+, D*(2007)0, etc. An empty string accepts all mothers.
 *  - MaxSearchDepth: Maximum "distance" in decay tree between gamma and mother.
 *  - MatchSearchDepth: Does the search depth need to match, or is it only a maximum?
 *  - MaxZ: Maximum z value the gamma conversion has to occur.
 *  - MinP: Minimum momentum of the products of the gamma conversion.
 *  - MinPT: Minimum transverse momentum of the products of the gamma conversion.
 *  - MinTheta: Minimum value of angle theta for conversion products
 *  - MaxTheta: Maximum value of angle theta for conversion products
 *
 *  @author Michel De Cian
 *  @date   2017-10-26
 */

class ConversionFilter : public GaudiAlgorithm {

 public: 
  /// Standard constructor
  ConversionFilter(const std::string& name, ISvcLocator* pSvcLocator);

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  

 private:

  std::string   m_mother;
  double        m_maxZ;
  double        m_minP;
  double        m_minPT;
  double        m_minTheta;
  double        m_maxTheta;
  int           m_motherID = -1;
  unsigned int  m_maxSearchDepth;
  bool          m_matchSearchDepth;
  
    
};
#endif // MCTRUTHMONITOR_H
