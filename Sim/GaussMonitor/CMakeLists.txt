################################################################################
# Package: GaussMonitor
################################################################################
gaudi_subdir(GaussMonitor v8r2)

gaudi_depends_on_subdirs(Event/GenEvent
                         Kernel/MCInterfaces
                         Phys/LoKiGen
                         Phys/LoKiMC
                         Sim/GaussTools
                         Det/VeloDet)

find_package(Boost)
find_package(CLHEP)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS} ${Geant4_INCLUDE_DIRS})

gaudi_add_module(GaussMonitor
                 src/*.cpp
                 INCLUDE_DIRS AIDA Kernel/MCInterfaces
                 LINK_LIBRARIES GenEvent LoKiGenLib LoKiMCLib GaussToolsLib VeloDetLib
                 GENCONF_PRELOAD GaussToolsGenConfHelperLib)

