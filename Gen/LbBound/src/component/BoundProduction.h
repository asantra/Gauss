#ifndef LBBOUND_BOUNDPRODUCTION_H 
#define LBBOUND_BOUNDPRODUCTION_H 1

// Gaudi.
#include "GaudiAlg/GaudiTool.h"
#include "Generators/IProductionTool.h"

/** 
 * Production tool to generate bound states. This is a pure virtual
 * class intended for generators which use minimum bias events to
 * produce bound states, e.g. deuteron production. The bindStates
 * methods must be implemented in derived classes.
 *  
 * @class  BoundProduction
 * @file   BoundProduction.h 
 * @author Philip Ilten
 * @date   2016-03-18
 */
class BoundProduction : public GaudiTool, virtual public IProductionTool {
public:

  /// Default constructor.
  BoundProduction(const std::string& type, const std::string& name,
		  const IInterface* parent);

  /// Initialize the tool.
  StatusCode initialize() override;

  /// Initialize the generator.
  StatusCode initializeGenerator() override;

  /// Finalize the tool.
  StatusCode finalize() override;

  /// Generate an event.
  StatusCode generateEvent(HepMC::GenEvent* theEvent,
				   LHCb::GenCollision* theCollision) override;

  /// Set particle stable.
  void setStable(const LHCb::ParticleProperty* thePP) override;

  /// Update a particle.
  void updateParticleProperties(const LHCb::ParticleProperty* thePP) override;

  /// Turn on fragmentation.
  void turnOnFragmentation() override;

  /// Turn off fragmentation.
  void turnOffFragmentation() override;

  /// Hadronize an event.
  StatusCode hadronize(HepMC::GenEvent* theEvent,
		LHCb::GenCollision* theCollision) override;

  /// Save the event record.
  void savePartonEvent(HepMC::GenEvent* theEvent) override;

  /// Retrieve the event record.
  void retrievePartonEvent(HepMC::GenEvent* theEvent) override;

  /// Print the running conditions.
  void printRunningConditions() override;

  /// Returns whether a particle has special status.
  bool isSpecialParticle(const LHCb::ParticleProperty* thePP) const override;

  /// Setup forced fragmentation.
  StatusCode setupForcedFragmentation(const int thePdgId) override;

  /// Initialize the bound process tool.
  virtual StatusCode boundInitialize();
  
  /// Initialize the bound process generator.
  virtual StatusCode boundInitializeGenerator();
  
  /// Finalize the bound process tool.
  virtual StatusCode boundFinalize();
  
protected:

  // Methods.
  /// Create the bound states.
  virtual StatusCode bindStates(HepMC::GenEvent *theEvent) = 0;

  // Properties.
  std::string m_beamToolName;   ///< The beam tool name.
  std::string m_prodToolName;   ///< The production tool name.

  // Members.
  int m_nEvents;                ///< Number of events.
  IProductionTool *m_prod;      ///< The production tool.
};

#endif // LBBOUND_BOUNDPRODUCTION_H
