from Configurables import Generation, Special, HijingProduction, MinimumBias, Inclusive, SignalPlain, SignalRepeatedHadronization

# Tuning for pPb/Pbp collisions
hijingTune = [
  "hiparnt hipr1 2 0.65",
  "hiparnt hipr1 6 0.12",

  "hijinginit bmin 0", #default 0.
  "hijinginit bmax 20" #default 0.
]

gen = Generation()
gen.addTool( Special )
gen.addTool( MinimumBias )
gen.addTool( Inclusive )
gen.addTool( SignalPlain )
gen.addTool( SignalRepeatedHadronization )

gen.Special.ProductionTool = "HijingProduction"
gen.MinimumBias.ProductionTool = "HijingProduction"
gen.Inclusive.ProductionTool = "HijingProduction"
gen.SignalPlain.ProductionTool = "HijingProduction"
gen.SignalRepeatedHadronization.ProductionTool = "HijingProduction"

gen.Special.addTool( HijingProduction )
gen.MinimumBias.addTool( HijingProduction )
gen.Inclusive.addTool( HijingProduction )
gen.SignalPlain.addTool( HijingProduction )
gen.SignalRepeatedHadronization.addTool( HijingProduction )

gen.Special.HijingProduction.Commands += hijingTune
gen.MinimumBias.HijingProduction.Commands += hijingTune
gen.Inclusive.HijingProduction.Commands += hijingTune
gen.SignalPlain.HijingProduction.Commands += hijingTune  
gen.SignalRepeatedHadronization.HijingProduction.Commands += hijingTune
