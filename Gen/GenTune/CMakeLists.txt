################################################################################
# Package: GenTune
################################################################################
gaudi_subdir(GenTune v3r0)

gaudi_depends_on_subdirs(Event/GenEvent
                         GaudiAlg
                         Gen/GENSER
                         Gen/Generators)

find_package(Boost)
find_package(FastJet)
find_package(HepMC)
find_package(ROOT)
find_package(Rivet)
find_package(YODA)

include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${HEPMC_INCLUDE_DIRS} ${FASTJET_INCLUDE_DIRS} ${RIVET_INCLUDE_DIRS} ${YODA_INCLUDE_DIRS})

gaudi_add_module(GenTune
                 src/*.cpp
                 INCLUDE_DIRS YODA Rivet FastJet
                 LINK_LIBRARIES YODA Rivet FastJet GenEvent GaudiAlgLib pythia6forgauss GeneratorsLib)


gaudi_add_test(QMTest QMTEST)
