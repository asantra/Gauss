
#include "Expression.h"


//// special unary functions ////

/*
   struct Exp : public IExpression {
   Exp( const Expression& other ) : m_expression( other ) {};
   virtual std::string to_string() const {
   return "exp(" + m_expression.to_string() +")";
   }
   virtual Expression d( const Parameter& div ) ;
   virtual std::complex<double> complexEval() const { return exp( m_expression.complexEval() ); }
   virtual double realEval() const { return exp( m_expression.realEval() ); }
   virtual void resolveDependencies( std::map < std::string, std::pair< unsigned int , double> >& dependencies ) {

   m_expression.resolveDependencies( dependencies );
   }
   operator Expression(){ return Expression( std::make_shared<Exp>(*this) ); }
   virtual void resolveEventMapping( const std::map < std::string, unsigned int>& evtMapping ){
   m_expression.resolveEventMapping( evtMapping );
   }

   Expression m_expression;
   };
   */
namespace AmpGen {
  struct Fmod : public IExpression {
    Expression A;
    Expression B;
    std::string to_string() const override {
      return "fmod("+A.to_string() +","+B.to_string() +")";
    }
    Fmod( const Expression& vA, const Expression& vB) : A(vA), B(vB) {}
    Expression d( const Parameter& div) override { return A.d( div ) ; }
    std::complex<double> complexEval() const override { return std::complex<double>(0,0) ; }
    double realEval() const override { return fmod( A.realEval(), B.realEval() ) ; }
    void resolveDependencies( std::map < std::string, std::pair< unsigned int , double> >& dependencies ) override {
      A.resolveDependencies( dependencies );
    }
    operator Expression(){ return Expression( std::make_shared<Fmod>(*this) ); }
    void resolveEventMapping( const std::map < std::string, unsigned int>& evtMapping ) override {
      A.resolveEventMapping( evtMapping );
      B.resolveEventMapping( evtMapping );
    }
    Expression conjugate() const override { return Expression( std::make_shared<Fmod>(*this) ) ; }
  };
}

