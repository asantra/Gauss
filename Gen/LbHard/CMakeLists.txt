################################################################################
# Package: LbHard
################################################################################
gaudi_subdir(LbHard v1r0p1)

gaudi_depends_on_subdirs(Gen/Generators
                         Gen/LbPythia
                         Gen/LbPythia8)

find_package(Boost)
find_package(HepMC)
find_package(Pythia8)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${HEPMC_INCLUDE_DIRS} ${PYTHIA8_INCLUDE_DIRS})

gaudi_add_library(LbHardLib
                  src/Lib/*.cpp
                  PUBLIC_HEADERS LbHard
                  LINK_LIBRARIES GeneratorsLib LbPythiaLib LbPythia8Lib)
