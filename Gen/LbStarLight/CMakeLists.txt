gaudi_subdir(LbStarLight v1r0)

gaudi_depends_on_subdirs(Gen/Generators)

find_package(Boost)
find_package(HepMC)
find_package(ROOT)
find_package(STARlight)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${STARLIGHT_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(LbStarLight
                 src/component/*.cpp
		 INCLUDE_DIRS Boost HepMC ROOT STARlight
                 LINK_LIBRARIES GeneratorsLib HepMC STARlight)
