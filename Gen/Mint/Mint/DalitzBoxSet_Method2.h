#ifndef DALITZ_BOX_SET_METHOD_TWO_HH
#define DALITZ_BOX_SET_METHOD_TWO_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:17:57 GMT

#include "Mint/DalitzBoxSet.h"
#include "Mint/DalitzEventPattern.h"
#include "TRandom.h"

#include "Mint/IEventGenerator.h"

class DalitzBoxSet_Method2
: public DalitzBoxSet
, virtual public MINT::IEventGenerator<IDalitzEvent>
{

  DalitzEventPattern _pat;

  double _maxHeight;
  void setBoxesToFlat();
  void getMaxHeight();
  void setEmptyBoxesHeight();
  void getReady() override;

  double eventsPDF(DalitzEvent& evt);
 public:
  DalitzBoxSet_Method2(const DalitzEventPattern& pat
		       , TRandom* rnd=gRandom);

  DalitzBoxSet_Method2(const DalitzBoxSet& boxSet
		       , const DalitzEventPattern& pat
		       );
  DalitzBoxSet_Method2(const DalitzBoxSet_Method2& other);

  double maxHeight() const;

  MINT::counted_ptr<DalitzEvent> tryEvent() override;
  MINT::counted_ptr<DalitzEvent> tryWeightedEvent() override;
  MINT::counted_ptr<IDalitzEvent> newEvent() override;

  //  virtual MINT::counted_ptr<DalitzEvent> generateEventForOwner();
  //  virtual DalitzEvent generateEvent();

  bool exhausted()const override {return false;}
  virtual ~DalitzBoxSet_Method2(){}
};

#endif
//
