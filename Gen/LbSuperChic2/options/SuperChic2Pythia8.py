# Import the necessary modules.
from Configurables import Generation, Special, MinimumBias, Pythia8Production
from Configurables import SuperChic2Production

# Add Pythia as minimum bias production tool.
Generation().addTool(MinimumBias)
Generation().MinimumBias.ProductionTool = "Pythia8Production"
Generation().MinimumBias.addTool(Pythia8Production)

# Add SuperChic2 as special production tool.
Generation().addTool(Special)
Generation().Special.ProductionTool = "SuperChic2Production"
Generation().Special.addTool(SuperChic2Production)
Generation().Special.SuperChic2Production.ShowerToolName = "Pythia8Production"
Generation().Special.PileUpProductionTool = "Pythia8Production/Pythia8PileUp"
Generation().Special.ReinitializePileUpGenerator  = False

