{% set used = [] -%}
{% macro section(labels) -%}
{% for mr in select_mrs(merge_requests, labels, used) %}
- {{mr.title}}, !{{mr.iid}} (@{{mr.author.username}}) {{find_tasks(mr)}}  
  {{mr.description|mdindent(2)}}
{% endfor %}
{%- endmacro %}

{{date}} {{project}} {{version}}
===

This version uses LHCb **v50r4**, Geant4 **v104r3p1**, Gaudi v32r0 and LCG 95 with
 HepMC 2.06.09 and Root 6.16.00

The generators used via LCG_95/MCGenerators) are
 pythia8 **240** (with LHCb new tune), lhapdf **6.2.1**, photos++ 3.56, 
 tauola++ 1.1.6b.lhcb, pythia 6.427.2,
 hijing 1.383bs.2, crmc 1.5.6 (epos), alpgen 2.1.4, powhegbox r3043.lhcb, 
 herwig++ 2.7.1, thepeg 2.1.1, 
 rivet 2.6.0, yoda 1.6.7.
 startlight r300  
and the internal implementation of:
 EvtGen with EvtGenExtras, AmpGen, Mint,
 BcVegPy and GenXicc, SuperChic2, LPair, MIB

The data packages specific for the simulation are 
GDMLData v1r* , XmlVis v2r* , DecFiles v30r* , 
LHAPDFSets v2r* , BcVegPyData ** v3r* ** , GenXiccData v3r* , PGunsData v1r* , 
MIBData v3r*. The version of Geant4Files is set by Geant4.


This version is released on `master`. 

It is to be used for productions referred to as **Sim10a** and is for early validation of Sim10 for Run1&2 and for Upgrade.

### New features
{{ section(['new feature']) }}

### Enhancements
{{ section(['enhancement']) }}

#### Generators
{{ section(['generators']) }}

#### Detector Simulation
{{ section(['detector sim']) }}

#### Fast Simulation
{{ section(['fastsim']) }}


### Bug fixes
{{ section(['bug fix']) }}

### Code modernisations and cleanups
{{ section(['cleanup', 'modernisation']) }}

### Monitoring changes and Changes to tests
{{ section(['monitoring', 'testing']) }}

### Other
{{ section([[]]) }}
